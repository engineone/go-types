package types

import (
	"os"
	"time"

	"github.com/engineone/xferspdy"
)

type SRename struct {
	From string `json:"from"`
	To   string `json:"to"`
}

// Diff represents the file difference as calculated by the filesystem module.
type Diff struct {
	Filepath  string      `json:"filepath"` // relative path
	Change    []byte      `json:"change"`   // content of change
	Rename    *SRename    `json:"rename,omitempty"`
	Mode      os.FileMode `json:"mode"`
	Type      EventType   `json:"type"`      // Type of change
	Digest    string      `json:"digest"`    // Hash of the file contents
	Timestamp time.Time   `json:"timestamp"` // the Timestamp
	IsRaw     bool        `json:"is_raw"`
	IsDir     bool        `json:"is_dir"`
	Size      uint64      `json:"size"`
	Language  string      `json:"language"`
}

type BinChange struct {
	Change      []byte
	Fingerprint *xferspdy.Fingerprint
}

type DiffIndex struct {
	Hash      string `gorm:"primary_key"`
	StateHash string `gorm:"index"`
	Filepath  string `gorm:"index"`
	Rename    *SRename
	Mode      os.FileMode
	Type      EventType
	Digest    string    `gorm:"index"`
	Timestamp time.Time `gorm:"index"`
	IsRaw     bool
	IsDir     bool
}

func (d *DiffIndex) TableName() string {
	return "DiffIndex"
}

type User struct {
	ID     string `json:"id" yaml:"id" gorm:"primary_key"`
	Name   string `json:"name" yaml:"name"`
	Email  string `json:"email" yaml:"email"`
	Avatar string `json:"avatar" yaml:"avatar"`
}

func (u *User) TableName() string {
	return "UserIndex"
}

// Signature is used to carry metadata about a human being that
// performed a certain action.
type Signature struct {
	Username string    `json:"username"`
	Name     string    `json:"name"`
	Email    string    `json:"email"`
	Date     time.Time `json:"date"`
}

// File is a representation of a file.
type File struct {
	Path           string      `json:"path"`
	Digest         string      `json:"digest"`
	Mode           os.FileMode `json:"mode"`
	CommitMessage  string      `json:"commit_message"`
	ChangeType     string      `json:"change_type"`
	Author         Signature   `json:"author"`
	Revision       string      `json:"revision"`
	Content        []byte      `json:"content"`
	Size           int64       `json:"size"`
	LastModified   time.Time   `json:"last_modified"`
	Created        time.Time   `json:"created"`
	LastModifiedBy string      `json:"last_modified_by"`
	IsDir          bool        `json:"is_dir"`
	IsBinary       bool        `json:"is_binary"`
}

// FileOpts are options used when reading files from the index.
type FileOpts struct {
	Content  bool   `json:"content"`
	Revision string `json:"revision"`
}

// FilesOpts are options used when getting a list of files
type FilesOpts struct {
	CurrentState bool `json:"current_state"` // if true only returns file from the current state
	ActiveFiles  bool `json:"active_file"`   // if true only returns files that exist in the file system
}

// Merge is a representation of a merge block
type Merge struct {
	MergeFrom string   `json:"merge_from"`
	Files     []string `json:"files"`
}

// EventType represents the types of events that can happen on a file
type EventType int

const (
	Create EventType = iota + 1
	Delete
	Modify
	Rename
	Attribute
)

func (e EventType) String() string {
	switch e {
	case Create:
		return "Create"
	case Delete:
		return "Delete"
	case Modify:
		return "Modify"
	case Rename:
		return "Rename"
	case Attribute:
		return "Attribute"
	default:
		return "udefined event type"
	}
}

type DeleteAll struct {
	Users map[string]int `json:"users"` // user ID is the map index
	Owner string         `json:"owner"`
}

type HasDeleted struct {
	User string `json:"user"`
}

type RunTimeStats struct {
	CPU    float64 `json:"cpu"`
	Memory float64 `json:"memory"`
	Repos  []*Repo `json:"repos"`
}

type GetAccessTokenReq struct {
	AuthToken string   `json:"auth_token"`
	LocalID   string   `json:"local_id"`
	Hostname  string   `json:"hostname"`
	ClientID  string   `json:"client_id"`
	Addresses []string `json:"addresses"`
	Port      int      `json:"port"`
}

type GetAccessTokenResp struct {
	RepoID      string     `json:"repo_id"`
	RepoName    string     `json:"repo_name"`
	AccessToken string     `json:"access_token"`
	Nodes       []RepoNode `json:"nodes"`
}

type GetNodesReq struct {
	AccessToken string   `json:"access_token"`
	ClientID    string   `json:"client_id"`
	Addresses   []string `json:"addresses"`
	Port        int      `json:"port"`
}

type GetNodesResp struct {
	Nodes []RepoNode `json:"nodes"`
}

type RepoNode struct {
	ClientID  string            `json:"client_id"`
	Addresses []string          `json:"addresses"`
	Port      int               `json:"port"`
	Hostname  string            `json:"hostname"`
	MountType MemberType        `bson:"mount_type" json:"mount_type"`
	PortMaps  map[string]string `bson:"port_maps" json:"port_maps"`
}

type MemberType int

const (
	AdminMember MemberType = iota + 1
	WorkerMember
)

func (m MemberType) String() string {
	switch m {
	case AdminMember:
		return "Admin"
	case WorkerMember:
		return "Worker"
	default:
		return "Undefined"
	}
}
