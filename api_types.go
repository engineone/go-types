package types

import (
	"time"

	"github.com/go-macaron/binding"
	macaron "gopkg.in/macaron.v1"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

type AddUser struct {
	User   *APIUser `json:"user" form:"user" binding:"Required"`
	Access string   `json:"access" form:"access" binding:"Required,In('read','write')"`
}

func (e AddUser) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) > 0 {
		toJSONError(ctx, errs)
	}
}

type Share struct {
	Username string `json:"username" form:"username" binding:"Required"`
}

func (e Share) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) > 0 {
		toJSONError(ctx, errs)
	}
}

type RepoDeleteOpts struct {
	Everywhere bool `json:"everywhere"`
	Sources    bool `json:"sources"`
}

type Repo struct {
	ID            string            `json:"id"`
	IDShort       string            `json:"id_short"`
	LocalID       string            `json:"local_id"`
	Name          string            `json:"name"`
	Path          string            `json:"path"`
	Published     bool              `json:"published"`
	VCS           string            `json:"vcs"`
	Collaborators map[string]string `json:"collaborators"`
	Running       bool              `json:"running"`
	Access        string            `json:"access"`
	Leadership    string            `json:"leadership"`
	Mode          string            `json:"mode"`
	AccessToken   string            `json:"access_token"`
}

type Select struct {
	RepoID string `json:"repo_id"`
}

type ApiFile struct {
	ID   string `json:"id"`
	Path string `json:"path"`
}

type Access int16

func (a Access) String() string {
	if a > 0 {
		return "write"
	}
	return "read"
}

type State struct {
	ID       string            `json:"id"`
	Name     string            `json:"name" form:"name" binding:"Required"`
	Parent   string            `json:"parent"`
	Active   bool              `json:"active"`
	CanRead  bool              `json:"can_read"`
	CanWrite bool              `json:"can_write"`
	Open     bool              `json:"open"`
	Access   map[string]string `json:"access"`
}

func (e State) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) > 0 {
		toJSONError(ctx, errs)
	}
}

type Status struct {
	Connected bool      `json:"connected"`
	Repo      string    `json:"repo"`
	Error     string    `json:"error"`
	Username  string    `json:"username"`
	Unsynced  int       `json:"unsynced"`
	LastSync  time.Time `json:"last_sync"`
}

type Auth struct {
	ID       string `json:"id"`
	Username string `json:"username" form:"username" binding:"Required"`
	Password string `json:"password" form:"password" binding:"Required"`
}

func (e Auth) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) > 0 {
		toJSONError(ctx, errs)
	}
}

type ValidationError struct {
	Fields  []string `json:"fields"`
	Type    string   `json:"type"`
	Message string   `json:"message"`
}

type Error struct {
	Code             int               `json:"code"`
	Reason           string            `json:"reason"`
	Help             string            `json:"help"`
	ValidationErrors []ValidationError `json:"validation_errors"`
}

type FilesHistoy struct {
	Path    string `json:"path" binding:"Required"`
	StateID string `json:"state_id"`
}

type ContentsAt struct {
	Path          string `json:"path" binding:"Required"`
	ChangeSetID   string `json:"change_set_id"`
	WorkingCopyID string `json:"state_id"`
}

func toJSONError(ctx *macaron.Context, errs binding.Errors) {
	e := &Error{
		Code:             400,
		Reason:           "Validation failed",
		Help:             "Check the input and corrected as directed",
		ValidationErrors: []ValidationError{},
	}

	for _, err := range errs {
		e.ValidationErrors = append(e.ValidationErrors, ValidationError{
			Fields:  err.FieldNames,
			Type:    err.Classification,
			Message: err.Message,
		})
	}

	ctx.JSON(400, e)
}

type AddNodeOpts struct {
	Address string `json:"address"` // Example: localhost:10111
}

type Node struct {
	ID            string                 `json:"id"`
	Type          string                 `json:"node_type"`
	Leadership    map[string]interface{} `json:"leadership"`
	LastBlockHash string                 `json:"last_block_hash"`
	Addresses     []string               `json:"addresses"`
	Port          int                    `json:"port"`
	Hostname      string                 `json:"hostname"`
	Connected     bool                   `json:"connected"`
}

type Conflict struct {
	Filepath string `json:"filepath"`
	OnState  string `json:"on_state"`
}

type APIAddUserRepoOpts struct {
	Email   string `json:"email"`
	Access  string `json:"access" form:"access" binding:"Required,In('read','write')"`
	Mounted bool   `json:"mounted"`
}

type APIDeleteUserRepoOpts struct {
	ClientID string `json:"client_id"`
}

type APIAddMeshUserRepoOpts struct {
	ID     string `json:"client_id"`
	Access string `json:"access" form:"access" binding:"Required,In('read','write')"`
}

type RepoStatus int8

type RepoRuntimeStatus int8

const (
	RepoStatusActive RepoStatus = iota + 1
	RepoStatusInactive
)

const (
	RepoRuntimeStatusRunning RepoRuntimeStatus = iota + 1
	RepoRuntimeStatusStopped
)

func (s RepoRuntimeStatus) String() string {
	if s == RepoRuntimeStatusRunning {
		return "running"
	}
	return "stopped"
}

func (s RepoStatus) String() string {
	switch s {
	case RepoStatusActive:
		return "active"
	default:
		return "inactive"
	}
}

type CreateAutoNodeOpts struct {
	Name         string   `json:"name"`
	Type         string   `json:"type"`
	LocalRepoIDs []string `json:"local_repo_ids"`
	NoExpire     bool     `json:"no_expire"`
}

// APIUser represents a user object to be sent to the client
type APIUser struct {
	ID       int32  `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Avatar   string `json:"avatar"`
	Token    string `json:"token,omitempty"`
	UserType string `json:"user_type"`
}

type ServerError struct {
	Title       string `bson:"title" json:"title"`
	Description string `bson:"description" json:"description"`
	Help        string `bson:"help" json:"help"`
}

type Server struct {
	ID           string            `json:"id"`
	Name         string            `json:"name"`
	Host         string            `json:"host"`
	Addresses    []string          `bson:"addresses" json:"addresses"`
	ExternalHost string            `json:"external_host"`
	PortMaps     map[string]string `bson:"port_maps" json:"port_maps"`
	Username     string            `json:"username"`
	Port         int               `json:"port"`
	NetworkPort  int               `bson:"network_port" json:"network_port"`
	Disk         int               `json:"disk"`
	Memory       int               `json:"memory"`
	CPU          int               `json:"cpus"`
	OS           string            `json:"os"`
	SetupStatus  string            `json:"setup_status"`
	OwnerID      string            `json:"owner_id"`
	APIToken     string            `bson:"api_token" json:"api_token"`
	CreatedAt    time.Time         `json:"created_at"`
	CreatedUnix  int64             `json:"created_unix"`
	NoExpire     bool              `json:"no_expire"`
	UpdatedAt    time.Time         `json:"updated_at"`
	RepoIDs      []string          `json:"mounts"`
	Error        *ServerError      `json:"error"`
	Type         string            `json:"type"`
}

type NodePostDiscovery struct {
	LocalRepoIDs []string `json:"local_repo_ids"`
}

type NewRepoOpts struct {
	Name           string        `json:"name"`
	Path           string        `json:"path"`
	GitRemote      string        `json:"git_remote"`
	ServerMode     bool          `json:"server_mode"`
	LocalID        string        `json:"local_id"`
	CreateNode     bool          `json:"create_node"`
	OwnerID        bson.ObjectId `json:"owner_id"`
	Active         RepoStatus    `json:"active"`
	WaitForMount   bool          `json:"wait_for_mount"`
	Auth           *Auth         `json:"auth"`
	IndexDirectory bool          `json:"index_directory"`
}

type NewRepoOptsOld struct {
	Name    string
	Path    string
	OwnerID bson.ObjectId
	Active  RepoStatus
	LocalID string `json:"local_id"`
}
type APILog struct {
	Hash      string `json:"hash"`
	Author    string `json:"author"`
	State     string `json:"state"`
	Date      string `json:"date"`
	Message   string `json:"message"`
	Committed bool   `json:"committed"`
	Type      string `json:"type"`
	FilePath  string `json:"file_path"`
}

// DeleteRepoOpts are passed when deleting a repo.
type DeleteRepoOpts struct {
	// Everwhere means the delete must be replicated to ever replica.
	Everywhere bool `json:"everywhere"`
	// Sources means that the repo directory will be deleted permanently.
	Sources bool `json:"sources"`
}

// DownloadRepoOpts is used when downloading a repo from another
// replica.
type DownloadRepoOpts struct {
	ID      string     `json:"id"`
	Path    string     `json:"path"`
	Address string     `json:"address"`
	Type    MemberType `json:"member_type"`
}

type RepoDownloadOpts struct {
	Path string
	Type MemberType
}

type UploadOpts struct {
	Path    string `json:"path"`
	Address string `json:"address"`
}

type ApplyOpts struct {
	HardState string `json:"hard_state"`
	SoftState string `json:"soft_state"`
}

type MergeState struct {
	Successful []string `json:"successful"`
	Failed     []string `json:"failed"`
}

type UpdateOpts struct {
	Auth   *Auth             `json:"auth"`
	Branch string            `json:"branch"`
	Stream string            `json:"stream"`
	Before string			 `json:"before"`
	After string			 `json:"after"`
	Author *object.Signature `json:"author"`
}
type CreateChangeOpts struct {
	Name    string `json:"name"`
	Mode    string `json:"mode"`
	Size    int    `json:"size"`
	State   string `json:"state"`
	IsDir   bool   `json:"is_dir"`
	UserID  string `json:"user_id"`
	Content string `json:"content"`
}
